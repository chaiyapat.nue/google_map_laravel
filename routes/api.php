<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RestaurantController;
 
Route::group(['prefix' => 'map'], function () {
    Route::post('google_map',  [RestaurantController::class, 'google_map_insert_db']);
    Route::post('create_update',  [RestaurantController::class, 'create_update_map']);
    Route::post('delete',  [RestaurantController::class, 'delete_map_test']);
    Route::post('list_name',  [RestaurantController::class, 'list_map_from_name']);
    Route::post('list_all',  [RestaurantController::class, 'list_map_all']);
    Route::post('test',  [RestaurantController::class, 'test']);
});

?>