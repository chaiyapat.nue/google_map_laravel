<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class RestaurantController extends BaseController
{
####################################################################################
    public function google_map_insert_db(Request $request){

        $r_lat  =   $request->lat;
        $r_lng  =   $request->lng;
        $radius =   $request->radius;

        if(!empty($r_lat) && !empty($r_lng) && !empty($radius)){
            $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurant&sensor=true&location=".$r_lat.",".$r_lng."&radius=".$radius."&key=AIzaSyAIVCA5SLR9zzGFSco1SEwFob-TrEJObqo";
        }
        else{
            $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurant&sensor=true&location=13.8104683,100.5387426&radius=500&key=AIzaSyAIVCA5SLR9zzGFSco1SEwFob-TrEJObqo";
        }

        $response = Http::post($url);
        $response_data = json_decode($response, true);
        $return = (object)$response_data;
        $results = $return->results;

        foreach($results as $val){

            $name           =   $val['name']; 
            $address        =   $val['formatted_address'];
            $type           =   $val['types'][0];
            $rating         =   $val['rating'];
            $user_rating    =   $val['user_ratings_total'];
            $lat            =   $val['geometry']['location']['lat'];
            $lng            =   $val['geometry']['location']['lng'];

            DB::table('location')->insert([
                'name'          => $name,
                'address'       => $address,
                'type'          => $type,
                'rating'        => $rating,
                'user_rating'   => $user_rating,
                'lat'           => $lat,
                'lng'           => $lng,
                'add_dated'     => date('Y-m-d H:i:s')
            ]);

            $return = [
                'status'    =>  200,
                'msg'       =>  'Create or Update Map Success'
            ];

        }

        return response()->json($return);

    }
####################################################################################
    public function list_map_from_name(Request $request){

        $name               =   $request->name;

        if(!empty($name)){
            $data = DB::table('location as loc')
                            ->select(
                                'loc.id','loc.name','loc.address','loc.type',
                                'loc.rating','loc.user_rating',
                                'loc.lat','loc.lng','loc.add_dated'
                            )
                    ->where('loc.name','like','%'.$name.'%')
                    ->get();

            $return = [
                'status'    =>  200,
                'msg'       =>  'Send List Success',
                'data'      =>  $data
            ];
        }
        else{
            $return = [
                'status'    =>  500,
                'msg'       =>  'Please Submit Name',
                'data'      =>  []
            ];
        }

        return $return;

    }
####################################################################################
    public function list_map_all(){

        $data = DB::table('location as loc')
                        ->select(
                            'loc.id','loc.name','loc.address','loc.type',
                            'loc.rating','loc.user_rating',
                            'loc.lat','loc.lng','loc.add_dated'
                        )
                ->get();
        
        if(sizeof($data) > 0){
            $return = [
                'status'    =>  200,
                'msg'       =>  'Send List Success',
                'data'      =>  $data
            ];
        }
        else{
            $return = [
                'status'    =>  500,
                'msg'       =>  'Send List Failed',
                'data'      =>  []
            ];
        }

        return $return;

    }
####################################################################################
    public function create_update_map_test(Request $request){

        $id                 =   $request->id; // 0 = insert >1 = id for update
        $name               =   $request->name;
        $address            =   $request->address;
        $type               =   $request->type;
        $rating             =   $request->rating;
        $user_rating        =   $request->user_rating;
        $lat                =   $request->lat;
        $lng                =   $request->lng;

        if(!empty($id)){
            DB::table('location')->where('id',$id)->update([
                'name'          => $name,
                'address'       => $address,
                'type'          => $type,
                'rating'        => $rating,
                'user_rating'   => $user_rating,
                'lat'           => $lat,
                'lng'           => $lng,
                'add_dated'     => date('Y-m-d H:i:s')
            ]);

            $return = [
                'status'    =>  200,
                'msg'       =>  'Create or Update Map Success'
            ];
        }
        else{
            DB::table('location')->insert([
                'name'          => $name,
                'address'       => $address,
                'type'          => $type,
                'rating'        => $rating,
                'user_rating'   => $user_rating,
                'lat'           => $lat,
                'lng'           => $lng,
                'add_dated'     => date('Y-m-d H:i:s')
            ]);

            $return = [
                'status'    =>  200,
                'msg'       =>  'Create or Update Map Success'
            ];
        }

        return $return;

    }
####################################################################################
    public function delete_map_test(Request $request){

        $id                 =   $request->id;

        if(!empty($id)){

            DB::table('location')->where('id',$id)->delete();

            $return = [
                'status'    =>  200,
                'msg'       =>  'Delete Map Success'
            ];
        }
        else{
            $return = [
                'status'    =>  500,
                'msg'       =>  'Please Submit Id'
            ];
        }

        return $return;

    }
####################################################################################
    public function test(){

        $data = DB::table('location as loc')
                    ->select(
                        'loc.id','loc.name','loc.address','loc.type',
                        'loc.rating','loc.user_rating',
                        'loc.lat','loc.lng','loc.add_dated'
                    )
                    ->get();

        $return = [
            'status'    =>  200,
            'msg'       =>  'Send List Success',
            'data'      =>  $data
        ];

        return  $return;
    }
}
