<?php

use App\Models\project_card;
use App\Services\FileService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

/**
 * -- folder s3 --
 * files/team_checkin           #รายงานกำลังพลทุกๆ วัน
 * files/album_stage            #รูปใบอัลบั้ม stage อัปเดตงาน
 * files/soc                    #รูปจุดเสี่ยง
 * files/team_user_profile      #รูปโปรไฟล์สมาชิกในทีม
 * files/team_user_card         #รูปบัตรประชาชนสมาชิกในทีม
 * files/check_safety           #เช็คความปลอดภัยก่อนเริ่มงานครั้งแรกของโปรเจค
 */

// image
if (!function_exists('image')) {
    function image($image)
    {
        $list_array = array(
            'not_found' => 'https://neo-project.s3.ap-southeast-1.amazonaws.com/picture/image-not-found.png',
            'default'   => 'https://neo-project.s3.ap-southeast-1.amazonaws.com/picture/default-picture.png',
        );
        return $list_array[$image];
    }
}

// custom message
if (!function_exists('msg')) {
    function msg($data)
    {
        $list_array = array(
            '401'                           => 'ไม่มีสิทธิ์เข้าถึง',
            '403'                           => 'ไม่ได้รับการอนุญาต',
            '404'                           => 'ไม่พบข้อมูล',
            'Please input value'            => 'กรุณากรอกค่า',
            'Created Successfully'          => 'สร้างสำเร็จแล้ว',
            'Updated Successfully'          => 'อัปเดตเรียบร้อยแล้ว',
            'Deleted Successfully'          => 'ลบเรียบร้อยแล้ว',
            'removeTeam'                    => 'ลบสมาชิกออกจากทีมแล้ว',
            'addTeam'                       => 'เพิ่มทีมให้กับสมาชิกแล้ว',
            'createUserTeam'                => 'ลงทะเบียนช่างสำเร็จ',
            'destroyUserTeam'               => 'ลบช่างสำเร็จ',
            'createCheckIn'                 => 'รายงานกำลังพลวันนี้แล้ว',
            'checkSafety_checked'           => 'ตรวจสอบความพร้อมทีมแล้ว',
            'Can not create a stage album'  => 'ไม่สามารถสร้างอัลบั้ม Stage ได้'
        );
        return $list_array[$data];
    }
}

// response ok
if (!function_exists('ok')) {
    function ok($msg)
    {
        return response()->json([
            'message' => msg($msg),
            'status'  => 200,
        ], 200);
    }
}

// response not ok
if (!function_exists('throwE')) {
    function throwE($msg = '404', $status = 400)
    {
        return response()->json([
            'message' => $msg,
            'status'  => $status,
        ], $status);
    }
}

if (!function_exists('MonthThai')) {
    function MonthThai($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));
        $strMonthFull = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $strMonthThai = $strMonthFull[$strMonth];
        return "$strMonthThai";
    }
}

if (!function_exists('formatDateThai')) {
    function formatDateThai($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("m", strtotime($strDate));
        $strDay = date("d", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));
        $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay/$strMonthThai/$strYear";
    }

    function formatDateThaiTime($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("m", strtotime($strDate));
        $strDay = date("d", strtotime($strDate));
        $strHour = date("H", strtotime($strDate));
        $strMinute = date("i", strtotime($strDate));
        $strSeconds = date("s", strtotime($strDate));
        $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay/$strMonthThai/$strYear $strHour:$strMinute:$strSeconds";
        // return "$strDay/$strMonth/$strYear $strHour:$strMinute:$strSeconds";
    }
}

if (!function_exists('genId')) {
    function genProjectId()
    {
        $db = new project_card;
        $query = $db->max('id') + 1;
        if (empty($query)) {
            $query = 1;
        }

        return $query;
    }

    function genWorkId($prefix)
    {
        $db = new project_card;

        $query = $db->select()
            ->where('work_id', 'like', "%" . $prefix . (date('Y') + 543) . date('md') . "%")
            ->count();
        $query += 1;
        $newnum = sprintf("%04d", $query);
        $code = $prefix . (date('Y') + 543) . date('md') . $newnum;
        return ($code);
    }
}

if (!function_exists('uploadToS3')) {
    function uploadFile($file, $folder)
    {
        $arr = array('file' => $file);
        $rules = array(
            'file' => 'mimes:jpeg,jpg,png,pdf,csv,txt,xlsx|required|max:10000', // max 10000kb
        );

        $validator = Validator::make($arr, $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        } else {
            $fileUpload = null;
            $file_name = null;
            if (!empty($file)) {
                $fileUpload = pathinfo((new FileService)->uploadFile($file, $folder));
                // $fileArray = explode('/', $fileUpload);
                // $fileKey = array_search("files", $fileArray);
                // $file_name = $fileArray[$fileKey + 1];
            }

            return ["file_name" => $fileUpload['basename'], "fileUpload" => $fileUpload['dirname'] . "/" . $fileUpload['basename']];
        };
    }

    // Milk Add แปะไว้ก่อน เดี๋ยวจะมาทำให้รองรับประเภท (image/file) และบังคับกรอก (nullable/required) หรือถ้าก้องจะทำก็ทำของก้องได้เลยนะ
    function uploadFileToS3($file, $path = 'unknown', $required = 'nullable', $file_type = 'image', $max = 10240)
    {
        /** Description variable
         *
         * $file_type   = image, file
         * $required    = required, nullable
         * $max         = max file size (default = 10240 #10 MB)
         *
         */

        $arr = array('file' => $file);
        $file_type = 'file' ? $mime = ',pdf' : $mime = null;

        $rules = ['"' . $required . '|' . $file_type . '|mimes:jpeg,png,jpg' . $mime . '|max:' . $max . '"'];

        $validator = Validator::make($arr, $rules);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        } else {
            $fileUpload = null;
            $file_name = null;
            if (!empty($file)) {
                $fileUpload = (new FileService)->uploadFile($file, 'files/' . $path);
                $fileArray = explode('/', $fileUpload);
                $fileKey = array_search("files", $fileArray);
                $file_name = $fileArray[$fileKey + 2];
            }
            return ["file_name" => $file_name, "url_file" => $fileUpload];
        }
    }
}

if (!function_exists('count_progress')) {
    function count_progress($request)
    {
        // count progress
        $count = DB::select('SELECT COUNT(id) AS closed,
                (SELECT COUNT(id)
                    FROM project_stages
                    WHERE project_id = ' . $request->project_id . ')
                        AS all_stage_type,
                ROUND(COUNT(id) * (100 / (SELECT COUNT(id)
                    FROM project_stages
                    WHERE project_id = ' . $request->project_id . ')),0)
                        AS progress,
                (SELECT IFNULL(stage_status,0)
                    FROM project_cards
                    WHERE id = ' . $request->project_id . ')
                        AS stage_status,
                (SELECT CASE WHEN stage_status = 1 THEN "ปิดงานทั้งหมดแล้ว"
                            WHEN stage_status = 2 THEN "มีให้แก้ไข"
                            ELSE "ยังไม่ได้ปิดงาน" END
                    FROM project_cards WHERE id = ' . $request->project_id . ')
                        AS stage_status_name,
                (SELECT work_check_note
                    FROM project_cards
                    WHERE id = ' . $request->project_id . ') AS comment,
                    "comment"                       AS f_comment,
                    "progress"                      AS f_progress,
                    "closed"                        AS f_closed,
                    "all_stage_type"                AS f_all_stage_type,
                    "all_project_stage_status"      AS f_all_project_stage_status,
                    "all_project_stage_status_name" AS f_all_project_stage_status_name
                FROM project_stages
                WHERE close_stage = 1 AND project_id = ' . $request->project_id . '');

        return $count;
    }

    if (!function_exists('findJobType')) {
        function findJobType($project_id)
        {
            $data = project_card::select('job_type')->where('id', $project_id)->get();
            return $data[0]['job_type'];
        }
    }
}
